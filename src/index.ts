import * as Vault from 'node-vault';
import main from './main';

const vault = Vault({
  requestOptions: {
    followAllRedirects: true
  }
})

const filters = (process.env.FILTERS||'onionvault').split(',')
  .map(f=>f.trim())
  .filter(f=>Boolean(f))

const limit = Number(process.env.LIMIT||0);

main(
  {
    filters: filters,
    limit: limit,
  },
  onion => {
    // console.log(JSON.stringify(onion))
    const {time,...data} = onion;
    return vault.write(`kv/onions/${onion.hostname}`,data)
    .then(()=>{
      console.log('saved',data.hostname)
    })
    .catch(err=>{
      console.error('Error while saving');
      console.error(err);
    })
  }
)
