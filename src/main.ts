import { spawn } from 'child_process';
import { load } from 'js-yaml';

interface OnionData {
  hostname: string;
  hs_ed25519_public_key: string;
  hs_ed25519_secret_key: string;
  time: Date;
}

export interface Parameters {
  limit?: number;
  filters: string[];
}

// const dockerImage = 'mwthink/mkp224o:2.16.2019';

const isFiltersInfo = (data:string): boolean => (
  data.substr(0,15) === 'sorting filters'
)

export default function main(params:Parameters,cb:(onion:OnionData)=>any){

  const proc = spawn(
    // `docker run --rm ${dockerImage}`, // To run with docker use this
    process.env['MKP224O_BIN']||'mkp224o',
    [
      ...params.filters,
      "-y",
      "-n",`${params.limit||0}`
    ],
    {shell:true}
  )

  proc.stderr.once('data',(data:Buffer)=>{
    if(isFiltersInfo(String(data))){
      // TODO Pretty format this data
      console.log(String(data))
    }
  })

  proc.on('close',(code)=>{
    if(code){}
    console.log('Application terminating');
    process.exit();
  })

  proc.stdout.on('data',(data:Buffer)=>{
    try {
      const parsed: OnionData = load(String(data));
      cb(parsed)
    }
    catch (err){
      console.error(err);
      process.exit()
    }
  })

  proc.stderr.on('data',(data)=>{
    if(isFiltersInfo(String(data))){return;}
  })
}
