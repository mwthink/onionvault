FROM node:11-alpine

ENV MKP224O_BIN="/usr/bin/mkp224o"

RUN apk add --no-cache libsodium
COPY --from=mwthink/mkp224o:2.16.2019 /mkp224o $MKP224O_BIN

WORKDIR /src

COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN npm run build

RUN yarn install --production && \
  rm -rf ./src tsconfig.json

ENTRYPOINT ["/usr/local/bin/npm","run"]
CMD ["start"]
